import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { switchMap, tap } from 'rxjs/operators';

import { CarsApiService } from '@api/cars-api.service';
import { Cars } from '@api/model/cars.model';
import { InitFispa, SetCarsList, SetCarsLoading } from './cars.actions';
import { Params } from '@angular/router';
import { NgxFispaService } from 'projects/ngx-fispa/src/public-api';

export class CarsStateModel {
  public cars!: Cars[];
  public totalCount!: number;
  public carsLoading!: boolean;
  public qp?: Params;
}

const defaults: CarsStateModel = {
  cars: [],
  totalCount: 0,
  carsLoading: false,
  qp: {},
};

@State<CarsStateModel>({
  name: 'cars',
  defaults,
})
@Injectable()
export class CarsState {
  constructor(
    private carsApi: CarsApiService,
  ) {}

  @Action(SetCarsLoading)
  setLoading({ patchState }: StateContext<CarsStateModel>, { carsLoading }: SetCarsLoading) {
    patchState({ carsLoading });
  }

  @Action(SetCarsList)
  setCars({ setState, dispatch }: StateContext<CarsStateModel>, { first, size }: SetCarsList) {
    dispatch(new SetCarsLoading(true));
    return this.carsApi.getCars(first, size).pipe(
      tap(({ data: cars, totalCount }) => setState({ cars, totalCount, carsLoading: false })),
    );
  }

  @Action(InitFispa)
  initFispa({ dispatch }: StateContext<CarsStateModel>, { fispaParams }: InitFispa) {
    return fispaParams.pipe(
      switchMap(qp => {
        console.log('qp', qp);
        return dispatch(new SetCarsList(qp.first, qp.size));
      }),
    );
  }

  // @Selector()
  // static cars(state: CarsStateModel): Cars[] {
  //   return state.cars;
  // }

  // @Selector()
  // static totalCount(state: CarsStateModel): number {
  //   return state.totalCount;
  // }
}

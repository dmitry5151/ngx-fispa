import { Params } from "@angular/router";
import { NgxFispaService } from "projects/ngx-fispa/src/public-api";
import { Observable } from "rxjs";

export class SetCarsLoading {
  static readonly type = '[Cars] Set cars loading';
  constructor(public carsLoading: boolean) { }
}

export class SetCarsList {
  static readonly type = '[Cars] Set cars list';

  constructor(
    public first = 0,
    public size = 10,
  ) {}
}

export class InitFispa {
  static readonly type = '[Cars] Init fispa';

  constructor(
    public fispaParams: Observable<Params>,
  ) {}
}

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { NgxFispaService } from 'projects/ngx-fispa/src/public-api';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [NgxFispaService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  public toolbarModel!: MenuItem[];

  ngOnInit(): void {
    this.toolbarModel = [
      {
        label: 'Showcase',
        items: [
          {
            label: 'Primeng Table',
            routerLink: 'showcase/primeng-table',
          },
        ],
      },
    ];
  }
}

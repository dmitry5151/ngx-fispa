import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsModule } from '@ngxs/store';
import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from '@shared/shared.module';
import { CarsState } from './state/cars.state';
import { NgxFispaModule } from 'projects/ngx-fispa/src/public-api';
import { Params } from '@angular/router';
import { FispaMappers } from 'projects/ngx-fispa/src/lib/models/mappers.interface';
import { FispaConfigurationService } from 'projects/ngx-fispa/src/lib/service/configuration.service';
import { ShowcaseModule } from './showcase/showcase.module';
import { LearnModule } from './learn/learn.module';

export function mapTo(qp: Record<string, any>): Params {
  return qp;
}

export function mapFrom(params: Params): Record<string, any> {
  return params;
}

export function fispaConfigurationFactory(): FispaConfigurationService {
  return new FispaConfigurationService({
    mapFromQueryParams: mapFrom,
    mapToQueryParams: mapTo,
  });
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    NgxsModule.forRoot([], {
      developmentMode: !environment.production,
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.production,
    }),
    NgxFispaModule.forRoot(fispaConfigurationFactory),
    ShowcaseModule,
    LearnModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { MenubarModule } from 'primeng/menubar';
import { SharedModule } from 'primeng/api';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    SharedModule,
    TableModule,
    MenubarModule,
  ],
})
export class PrimengModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimengModule } from './primeng/primeng.module';
import { MyAsyncPipe } from './pipes/my-async.pipe';



@NgModule({
  declarations: [
    MyAsyncPipe,
  ],
  imports: [
    CommonModule,
    PrimengModule,
  ],
  exports: [
    PrimengModule,
    MyAsyncPipe,
  ],
})
export class SharedModule { }

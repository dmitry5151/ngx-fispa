import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Pipe({
  name: 'myAsync',
  pure: false,
})
export class MyAsyncPipe implements PipeTransform, OnDestroy {

  resultValue: unknown | 'null' = 'null';

  sub!: Subscription | null;

  constructor(private cd: ChangeDetectorRef) {}

  transform(value$: Observable<unknown> | Subject<unknown>, ...args: unknown[]): unknown {
    // console.log('transform');
    if (!this.sub) {
      this.sub = value$.pipe(
        distinctUntilChanged(),
      ).subscribe(val => {
        // console.log('get val');
        this.resultValue = val;
        this.cd.detectChanges();
      });
    }
    return this.resultValue;
  }

  ngOnDestroy(): void {
    console.log('destroy unsubscribe');
    this.sub?.unsubscribe();
    this.sub = null;
    this.resultValue = 'null';
  }

}

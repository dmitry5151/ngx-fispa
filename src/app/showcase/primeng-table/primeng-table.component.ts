/* eslint-disable @angular-eslint/component-selector */
import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { NgxFispaService } from '@fispa/service/ngx-fispa.service';
import { Select, Store } from '@ngxs/store';
import { InitFispa } from '@state/cars.actions';
import { CarsState, CarsStateModel } from '@state/cars.state';
import { LazyLoadEvent } from 'primeng/api';
import { Table } from 'primeng/table';
import { Observable, of, delay, tap } from 'rxjs';

@Component({
  selector: 'app-primeng-table',
  templateUrl: './primeng-table.component.html',
  styleUrls: ['./primeng-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [NgxFispaService],
})
export class PrimengTableComponent implements OnInit {
  @ViewChild(Table) private table!: Table;
  @Select(CarsState) carsState$!: Observable<CarsStateModel>;

  public testAsync$ = of('test').pipe(delay(3000), tap(() => console.log('delay')));

  public name!: string;
  public lastName!: string;

  constructor(
    private store: Store,
    private fispa: NgxFispaService,
  ) {}

  ngOnInit(): void {

    this.store.dispatch(new InitFispa(this.fispa.init()));
    // this.fispa.init().subscribe(qp => console.log('table qp', qp));
    const arr = ['Вася', 'Пупкин'];
    const obj = {
      name: 'Вася',
      lastName: 'Пупкин',
    };
    ({ name: this.name, lastName: this.lastName } = obj);
  }

  getCars(e: LazyLoadEvent): void {
    console.log('get next page');
    // this.store.dispatch(new SetCarsList(e.first, e.rows));
    this.fispa.next({
      first: e.first,
      rows: e.rows,
    });
  }
}

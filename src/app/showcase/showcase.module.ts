import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { SharedModule } from '@shared/shared.module';
import { CarsState } from '@state/cars.state';

import { PrimengTableComponent } from './primeng-table/primeng-table.component';
import { ShowcaseComponent } from './showcase.component';



@NgModule({
  declarations: [
    PrimengTableComponent,
    ShowcaseComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxsModule.forFeature([CarsState]),
  ],
})
export class ShowcaseModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinkedListComponent } from './learn/linked-list/linked-list.component';
import { ShowcaseComponent } from './showcase/showcase.component';
import { PrimengTableComponent } from './showcase/primeng-table/primeng-table.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'showcase',
  },
  {
    path: 'showcase',
    component: ShowcaseComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'primeng-table',
      },
      {
        path: 'primeng-table',
        component: PrimengTableComponent,
      },
    ],
  },
  {
    path: 'learn',
    children: [
      {
        path: 'linked-list',
        component: LinkedListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

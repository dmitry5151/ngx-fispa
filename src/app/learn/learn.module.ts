import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkedListComponent } from './linked-list/linked-list.component';
import { SharedModule } from '@shared/shared.module';



@NgModule({
  declarations: [
    LinkedListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
})
export class LearnModule { }

export interface DataModel<T> {
  data: T;
  totalCount: number;
}

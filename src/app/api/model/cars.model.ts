export interface CarsData {
  data: Cars[];
}

export interface Cars {
  vin: string;
  brand: string;
  year: number;
  color: string;
}

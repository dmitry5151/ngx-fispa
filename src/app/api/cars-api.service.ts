import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map, shareReplay } from 'rxjs/operators';

import { Cars, CarsData } from './model/cars.model';
import { DataModel } from './model/data.model';

@Injectable({
  providedIn: 'root',
})
export class CarsApiService {
  private cars: Observable<Cars[]>;

  constructor(
    private http: HttpClient,
  ) {
    this.cars = this.http.get<CarsData>('assets/data/cars-large.json').pipe(
      map(cars => cars.data),
      shareReplay(1),
    );
  }

  getCars(start = 0, size = 10): Observable<DataModel<Cars[]>> {
    const end = start + size;
    return this.cars.pipe(
      map(cars => ({
        data: cars.slice(start, end),
        totalCount: cars.length,
      } as DataModel<Cars[]>)),
      delay(2000),
    );
  }
}

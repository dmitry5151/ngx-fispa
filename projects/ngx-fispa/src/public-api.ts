/*
 * Public API Surface of ngx-fispa
 */

export * from './lib/service/ngx-fispa.service';
export * from './lib/ngx-fispa.module';

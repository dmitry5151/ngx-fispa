import { ModuleWithProviders, NgModule } from '@angular/core';

import { FispaConfigurationService } from './service/configuration.service';



@NgModule({
  imports: [],
  exports: [],
})
export class NgxFispaModule {
  public static forRoot(mappersFactory: () => FispaConfigurationService): ModuleWithProviders<NgxFispaModule> {
    return {
      ngModule: NgxFispaModule,
      providers: [
        {
          provide: FispaConfigurationService,
          useFactory: mappersFactory,
        },
      ],
    };
  }
}

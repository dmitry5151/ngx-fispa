import { Params } from "@angular/router";

export interface FispaMappers {
  mapToQueryParams: (params: Record<string, any>) => Params;
  mapFromQueryParams: (queryParams: Params) => Record<string, any>;
}

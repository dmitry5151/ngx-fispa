import { ActivatedRoute } from "@angular/router";

export interface FispaConfig {
  // filtersConfig?: FiltersModel<T, F>[];
  sortConfig?: unknown[];
  searchConfig?: unknown;
  route: ActivatedRoute;
  // context: C;
  rawParams?: boolean;
}

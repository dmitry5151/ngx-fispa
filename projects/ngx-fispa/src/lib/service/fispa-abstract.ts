import { Observable, ReplaySubject } from "rxjs";
import { FispaConfig } from "../models/fispa.interface";
import { Params } from "@angular/router";

export abstract class FispaAbstract {

  public abstract init(config?: FispaConfig): Observable<Params>;

  public abstract next(qp: Params): void;

  // public abstract search(): void;
  // public abstract sort(): void;
  // public abstract getData(): void;

  // protected abstract filter(): void;
}

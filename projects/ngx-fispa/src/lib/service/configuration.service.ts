import { FispaMappers } from "../models/mappers.interface";

export class FispaConfigurationService {
  mappers!: FispaMappers;
  constructor(mappers: FispaMappers) {
    this.mappers = mappers;
  }
}

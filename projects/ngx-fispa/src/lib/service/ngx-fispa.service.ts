import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

import { FispaConfig } from '../models/fispa.interface';
import { FispaAbstract } from './fispa-abstract';
import { FispaConfigurationService } from './configuration.service';

@Injectable()
export class NgxFispaService extends FispaAbstract {
  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected configuration: FispaConfigurationService,
  ) {
    super();
    if (this.configuration.mappers) {
      console.log('mappers', this.configuration.mappers);
    }
  }

  public init(config?: FispaConfig): Observable<Params> {
    return this.route.queryParams.pipe(
      filter(qp => Object.keys(qp).length > 0),
      tap(qp => console.log('[fispa] query params', qp)),
    );
  }

  public next(queryParams: Params): void {
    const extras: NavigationExtras = {
      relativeTo: this.route,
      queryParams,
    };

    this.router.navigate(['./'], extras);
  }
}

# Fispa

## Концепция

В задачу ФИСПА сервиса входит отслеживание и работа с параметрами фильтрации, сортировки и пагинации. При переходе
пользователя на другую страницу, изменении параметров сортировки или фильтров - состояние сервиса должно быть изменено
до актуального.

Для работы с параметрами ФИСПА необходимо задать их имена, чтобы отличить и изолировать от других параметров запроса.


## Требования к разработке

 - Unit-тесты на Jest
 - e2e-тесты (Cypress)
 - SOLID

## Базовая конфигурация



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
